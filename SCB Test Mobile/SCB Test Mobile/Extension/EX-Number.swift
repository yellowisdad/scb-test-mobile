//
//  EX-Number.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation

extension Float {
    func commaNumber(_ digits: Int = 2) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.maximumFractionDigits = digits
        numberFormatter.minimumFractionDigits = digits
        
        return numberFormatter.string(from: NSNumber(value:self))!
}
}
