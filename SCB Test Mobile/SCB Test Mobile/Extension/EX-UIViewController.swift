//
//  EX-UIViewController.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {   
    
    func showAlertOK(_ title: String, message: String?, action: String, completion: (() -> Void)? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: action, style: .default) { (action) in 
            if action.isEnabled {
                completion?() 
            }
        }
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: completion)
    }
    
    
    func dismissOrPop(){
        if let navigationController = navigationController {
            _ = navigationController.popViewController(animated: true);
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
