//
//  EX-String.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func mpFind(_ text: String) -> Bool {
        return (self.range(of:text) != nil)
    }
}
