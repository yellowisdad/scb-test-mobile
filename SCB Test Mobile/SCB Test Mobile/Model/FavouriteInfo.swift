//
//  FavouriteInfo.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation
import ObjectMapper


class FavouriteInfo : Mappable {
    
    var products : [ProductInfo] = []
    
    required init?(map: Map){}
    
    init(_ info : [ProductInfo] ) {
        self.products = info
    }
    
    func mapping(map: Map) {
        products <- map["products"]
        
    }
}
