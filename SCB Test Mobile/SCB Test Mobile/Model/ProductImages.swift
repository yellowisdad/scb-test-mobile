//
//  ProductImages.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation
import ObjectMapper

public struct ProductImagesInfo : Mappable {
    var id: Int = 0
    var url: String?
    var mobile_id: Int = 0
    
    public init?(map: Map) { }
    
    mutating public func mapping(map: Map) {
        id <- map["id"]
        url <- map["url"]
        mobile_id <- map["mobile_id"]
        
        if let _url = url, !_url.mpFind("http") {
                self.url = "https://" + _url
        }
    }
}
