//
//  ProductInfo.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

/*
"thumbImageURL": "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg",
"price": 179.99,
"rating": 4.9,
"name": "Moto E4 Plus",
"description": "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.",
"brand": "Samsung",
"id": 1
*/

import Foundation
import ObjectMapper

public struct ProductInfo : Mappable {
    var id: Int = 0
    var thumbImageURL: String?
    var price: Float = 0.0
    var rating: Float = 0.0
    var name: String = "Unknown"
    var description: String = ""
    var brand: String = ""

    public init?(map: Map) { }
    
    mutating public func mapping(map: Map) {
        id <- map["id"]
        thumbImageURL <- map["thumbImageURL"]
        price <- map["price"]
        rating <- map["rating"]
        name <- map["name"]
        description <- map["description"]
        brand <- map["brand"]
        
        if let _thumbImageURL = thumbImageURL, !_thumbImageURL.mpFind("http") {
            self.thumbImageURL = "https://" + _thumbImageURL
        }
    }
}
