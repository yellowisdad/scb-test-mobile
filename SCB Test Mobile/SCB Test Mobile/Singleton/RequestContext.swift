//
//  RequestContext.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class RequestContext: NSObject {
    
    let mainURL = ServiceURL(development: "https://scb-test-mobile.herokuapp.com/api",
                             staging: "https://translate.google.co.th",
                             production: "https://scb-test-mobile.herokuapp.com/api")
    
    public func getRequest(_ method: HTTPMethod, 
                           path: String, 
                           contentType : RequestContentType = .json,
                           header: [String : String] = [:], 
                           body: Any? = nil, 
                           domain: ServiceURL
        //,token: Bool = false
        ) -> URLRequest? {
        
        let baseURL : String = domain.get(AppGlobal.shared.environment).absoluteString
        var request = URLRequest(url: URL(string: baseURL + path)!)
        //print(baseURL + path)
        
        request.httpMethod = method.rawValue
        //request.allHTTPHeaderFields = header
        
        request.addValue( contentType.description , forHTTPHeaderField: "Content-Type")
        request.cachePolicy = .reloadIgnoringCacheData
        //        if token {
        //            request.addValue( "Bearer \(AppGlobal.storage.loginInfo().token)" , forHTTPHeaderField: "Authorization")
        //        }
        
        if let requestBody = body { 
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: requestBody, options: JSONSerialization.WritingOptions())
                return request
            }
            catch {
                return nil
            }
        } else {
            return request
        }
    }
}



struct ServiceURL {
    private(set) var development: URL
    private(set) var staging: URL
    private(set) var production: URL
    
    init (development: String, staging: String, production: String) {
        self.development = URL(string: development)!
        self.staging = URL(string: staging)!
        self.production = URL(string: production)!
    }
    
    func get(_ environment: AppEnvironment) -> URL {
        switch environment {
        case .development:
            return self.development
        case .staging:
            return self.staging
        default:
            return self.production
        }
    }
}


public enum ConnectionResult {
    case success(Data)
    case failure(Error)
}

public enum RequestMethod: Int, CustomStringConvertible {
    case get = 0
    case post
    case patch
    
    public static let count: Int = 3
    public var description: String {
        switch self {
        case .get : return "GET"
        case .post : return "POST"
        case .patch : return "PATCH"
        }
    }
}

public enum RequestContentType: Int, CustomStringConvertible {
    case json = 0
    case web = 1
    case from = 2
    public static let count: Int = 1
    public var description: String {
        switch self {
        case .json : return "application/json"
        case .web : return "application/x-www-form-urlencoded"
        case .from : return "application/form-data"
        }
    }
}

