//
//  StorageContext.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation


class StorageContext: NSObject {
    
    private let storage = UserDefaults.standard
    
    public func clearStorage() {
        let domain = Bundle.main.bundleIdentifier!
        storage.removePersistentDomain(forName: domain)
        storage.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
    
    
    private let FAVOURITE_KEY = "FAVOURITE_KEY"
        
    public func addFavourite(_ info: [ProductInfo]){
        let dataInfo = FavouriteInfo(info)
        let archiverData = NSKeyedArchiver.archivedData(withRootObject: dataInfo.toJSON() )
        storage.set(archiverData , forKey: FAVOURITE_KEY)
        storage.synchronize()
    }
    
    public func getAllFavourite() -> [ProductInfo] {
        if let placesData = storage.object(forKey: FAVOURITE_KEY) as? NSData {
            if let archiverData = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? [String: Any] {
                return FavouriteInfo(JSON: archiverData)!.products
            } else {
                return []
            }
        } else {
            return []
        }
    }
}
