//
//  AppGlobal.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation

class AppGlobal {
    
    static var shared = AppGlobal()
    fileprivate(set) internal var environment: AppEnvironment
    fileprivate(set) internal var request: RequestContext
    fileprivate(set) internal var service: ServiceAPIContext
    fileprivate(set) internal var storage: StorageContext
    
    convenience init() {
        self.init(environment: .production)
    }
    
    init(environment: AppEnvironment) {
        self.environment = environment
        self.request = RequestContext()
        self.service = ServiceAPIContext()
        self.storage = StorageContext()
    }
}

enum AppEnvironment {
    case development
    case staging
    case production
    
    func toString () -> String {
        switch self {
        case .development:
            return "Development"
        case .staging:
            return "Staging"
        case .production:
            return "Production"
        }
    }
}
