//
//  ServiceAPIContext.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class ServiceAPIContext: NSObject {

    
    public func getMobiles(completion: @escaping ([ProductInfo]?,Error?) -> ()){
        if let request = AppGlobal.shared.request.getRequest(.get, 
                                                             path: "/mobiles/",
                                                            domain: AppGlobal.shared.request.mainURL) {
            Alamofire.request(request).responseArray { (response: DataResponse<[ProductInfo]>) in
                switch response.result {
                case .success(let data):
                    completion(data,nil)
                case .failure(let e):
                    completion(nil,e)
                }
            }
            
        }
    }
    
    public func getMobileImages(_ id: Int, completion: @escaping ([ProductImagesInfo]?,Error?) -> ()){
        if let request = AppGlobal.shared.request.getRequest(.get, 
                                                             path: "/mobiles/\(id)/images/",    
                                                             domain: AppGlobal.shared.request.mainURL) {
            Alamofire.request(request).responseArray { (response: DataResponse<[ProductImagesInfo]>) in
                switch response.result {
                case .success(let data):
                    completion(data,nil)
                case .failure(let e):
                    completion(nil,e)
                }
            }
            
        }
    }
    

}
