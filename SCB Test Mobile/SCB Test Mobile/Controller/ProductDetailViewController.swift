//
//  ProductDetailViewController.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import UIKit
import SimpleImageViewer

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var item : ProductInfo?
    var images : [ProductImagesInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableView.automaticDimension
        
        if let product = item {
            self.navigationItem.title = product.name
            
            AppGlobal.shared.service.getMobileImages(product.id) { item, error in
                if let image = item {
                    self.images = image
                    self.tableView.reloadData()
                } else if let e = error {
                    self.showAlertOK("ผิดพลาด", message: e.localizedDescription , action: "ตกลง")
                }
            }
        }
    }

}

extension ProductDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "item", for:indexPath) as! ProductDetailTableViewCell
        
        if let item = self.item {
            cell.desLabel.text = item.description
            cell.priceLabel.text = "Price: $\(item.price.commaNumber())"
            cell.rateLabel.text = "Rating: \(item.rating.commaNumber(1))"
            cell.images = images
            cell.callSelectImage = {
                let configuration = ImageViewerConfiguration { config in
                    config.imageView = cell.seekShowImageView
                }
                self.present(ImageViewerController(configuration: configuration), animated: true)
            }
        }

        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1  
    }
}
