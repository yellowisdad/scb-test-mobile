//
//  ProductListViewController.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import UIKit
import Kingfisher
import PKHUD

enum ProductSort {
    case null
    case low_hight
    case high_low
    case rate
}
class ProductListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var emptry: UILabel!
    
    private var topRefreshControl: UIRefreshControl!
    
    var products : [ProductInfo] = []
    var favourites : [ProductInfo] = []
    
    var productSort : ProductSort = .null
    
    var items : [ProductInfo] = [] {
        didSet {
            emptry.isHidden = items.count > 0
            emptry.text = "No Data"
            
            //reload when init
            tableView.reloadData()
                
            
        }
    }
    
    var isAllState : Bool = true { //true=all , false=favourite 
        didSet {
            if isAllState {
                items = self.sortProduct(item: products, sort: self.productSort) 
            } else {
                items = self.sortProduct(item: favourites, sort: self.productSort) 
            }
        }
    }
    
        
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableView.automaticDimension

        //reload item
        topRefreshControl = UIRefreshControl()
        topRefreshControl.addTarget(self, action: #selector(self.topRefresh(_:)), for: .valueChanged)
        tableView.addSubview(topRefreshControl)
    
        favourites = AppGlobal.shared.storage.getAllFavourite()
        reloadContent()

    }
    
    @objc private func topRefresh(_ sender:AnyObject) {
        productSort = .null
        reloadContent()
    }
    
    func checkIsFavourite(_ id : Int) -> Int? {
        if let index = favourites.index(where: { $0.id == id }) {
            return index
        } else {
            return nil 
        }
    }
    
    func addFavourite(item : ProductInfo, isFavourite: Bool){
        if isFavourite {
            favourites.append(item)
                AppGlobal.shared.storage.addFavourite(self.favourites)
        } else {
            if let index = checkIsFavourite(item.id) {
                favourites.remove(at: index)
                AppGlobal.shared.storage.addFavourite(self.favourites)
                reloadContent()
            }
        }
        HUD.flash(.success, delay: 0.1)
    }
    func reloadContent(){
        if isAllState {
            AppGlobal.shared.service.getMobiles() { item, error in
                self.topRefreshControl.endRefreshing()
                if let product = item {
                    self.products = product
                    self.items =  product
                } else if let e = error {
                    self.showAlertOK("ผิดพลาด", message: e.localizedDescription , action: "ตกลง")
                }
            }
        } else {
            favourites = AppGlobal.shared.storage.getAllFavourite()
            items = self.sortProduct(item: favourites, sort: self.productSort) 
            self.topRefreshControl.endRefreshing()
        }
        
       
    }
    
    func sortProduct(item: [ProductInfo], sort : ProductSort) -> [ProductInfo]{ 
        //set new sort state
        self.productSort = sort
        
        //sort product
        switch sort {
        case .low_hight:
            return item.sorted(by: { $0.price < $1.price })
        case .high_low:
            return item.sorted(by: { $0.price > $1.price })
        case .rate:
            return item.sorted(by: { $0.rating > $1.rating })
        default:
            return item
        }
        
    }
    @IBAction func didSortButtonPress(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Sort", message: nil, preferredStyle: .alert)
        let low_hight = UIAlertAction(title: "Price low to high", style: .default) { (action) in 
            self.items = self.sortProduct(item: self.items, sort: .low_hight)
        }
        let hight_low = UIAlertAction(title: "Price high to low", style: .default) { (action) in 
            self.items = self.sortProduct(item: self.items, sort: .high_low)
        }
        let rating = UIAlertAction(title: "Rating", style: .default) { (action) in 
            self.items = self.sortProduct(item: self.items, sort: .rate)
        }
        let cancel = UIAlertAction(title: "Cancel" , style: .cancel, handler: { (self) in
            
        })
        alertController.addAction(low_hight)
        alertController.addAction(hight_low)
        alertController.addAction(rating)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func didAllButtonPress(_ sender: UIButton) {
        sender.setTitleColor(.black, for: .normal)
        favouriteButton.setTitleColor(.lightGray, for: .normal)
        
        isAllState = true
    }
    
    @IBAction func didFavouriteButtonPress(_ sender: UIButton) {
        sender.setTitleColor(.black, for: .normal)
        allButton.setTitleColor(.lightGray, for: .normal)
        
        isAllState = false
    }
    
    
}

extension ProductListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        let alertController = UIAlertController(title: "Delete?", message: "Confirm to Delete \(item.name) from your favourite list?", preferredStyle: .alert)
        let confirm = UIAlertAction(title: "Confirm", style: .default) { (action) in 
            self.addFavourite(item: item, isFavourite: false)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in 
            
        }
        alertController.addAction(confirm)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return !isAllState 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailView = storyBoard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        detailView.item = items[indexPath.row]
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "item", for:indexPath) as! ProductItemTableViewCell
        
        let item = items[indexPath.row]
        cell.nameLabel.text = item.name
        cell.desLabel.text = item.description
        cell.priceLabel.text = "Price: \(item.price.commaNumber())"
        cell.rateLabel.text = "Rating: \(item.rating.commaNumber(1))"
        if let urlImage = item.thumbImageURL, let url = URL(string: urlImage) {
            cell.contentImage.kf.setImage(with: url)
        }
        
        //hide fevbutton when fevState
        if isAllState {
        cell.isFavourite = checkIsFavourite(item.id) != nil
        }
        
        //press button event
        cell.callFavourite = { isFavourite in
            self.addFavourite(item: item, isFavourite: isFavourite)
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1  
    }
}
