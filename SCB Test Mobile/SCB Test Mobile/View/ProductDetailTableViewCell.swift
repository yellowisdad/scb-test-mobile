//
//  ProductDetailTableViewCell.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import UIKit
//slide image
import FSPagerView

class ProductDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var seekShowImageView: UIImageView!
    @IBOutlet private weak var contentImageHeight: NSLayoutConstraint!
    @IBOutlet private weak var contentImageView: FSPagerView!
    
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    var callSelectImage : (() -> ())?
    
    var images : [ProductImagesInfo] = [] {
        didSet {
            contentImageView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //The images should be horizontally scrollable and should consume 35% of the screen height of the device.
        contentImageHeight.constant = UIScreen.main.bounds.size.height * 0.35
        
        contentImageView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        contentImageView.delegate = self
        contentImageView.dataSource = self
        
        contentImageView.isInfinite = true
        contentImageView.automaticSlidingInterval = 3.0
        contentImageView.interitemSpacing = 4.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension ProductDetailTableViewCell: FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        callSelectImage?()
    }
    
    
}

extension ProductDetailTableViewCell: FSPagerViewDataSource {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return images.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)

        let data = self.images[index]
        cell.imageView?.contentMode = .scaleAspectFit
        
        if let imageUrl = data.url, let url = URL(string: imageUrl) {
            cell.imageView?.kf.setImage(with: url, placeholder: UIImage(named: "loaging_image"), options: [.transition(.fade(1)),
                                                                                                           .cacheOriginalImage])
            self.seekShowImageView.kf.setImage(with: url)
        } else {
            cell.imageView?.image = UIImage(named: "no-image")
            self.seekShowImageView.image = UIImage(named: "no-image")
        }
    
        cell.contentView.layer.shadowRadius = 0
        cell.contentView.layer.backgroundColor = UIColor.white.cgColor
        cell.clipsToBounds = true
        return cell
    }
}
