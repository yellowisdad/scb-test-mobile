//
//  ProductItemTableViewCell.swift
//  SCB Test Mobile
//
//  Created by Methawee Punkaew on 27/1/19.
//  Copyright © 2019 Methawee Punkaew. All rights reserved.
//

import UIKit

class ProductItemTableViewCell: UITableViewCell {

    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    @IBOutlet private weak var favButton: UIButton!
    
    var callFavourite : ((Bool) -> ())?
    
    var isFavourite : Bool = false {
        didSet {
            favButton.isHidden = false
            if isFavourite {
                favButton.setImage(UIImage(named: "ic_start_full"), for: .normal)
            } else {
                favButton.setImage(UIImage(named: "ic_start_bor"), for: .normal)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        favButton.isHidden = true
        nameLabel.text = "Loading..."
        desLabel.text = "Loading..."
        priceLabel.text = "Price: $0"
        rateLabel.text = "Rating: 0.0"
        favButton.setImage(UIImage(named: "ic_start_bor"), for: .normal)
        contentImage.image = UIImage(named: "loaging_image")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didFavouriteButtonPress(_ sender: UIButton) {
        isFavourite = !isFavourite
        callFavourite?(isFavourite)
    }

    
}
